import React from 'react';
import { render, screen, waitFor, within } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import Chance from 'chance';
import * as path from 'path';
import * as fs from 'fs';

import App from './App';

const chance = new Chance();
const specialities = ['Excavation', 'Plumbing', 'Electrical'];
const companies = specialities.flatMap((speciality) => [
  {
    guid: chance.guid(),
    name: chance.company(),
    logo: 'https://avatar-url.none/get',
    speciality: speciality,
    city: chance.city(),
  },
  {
    guid: chance.guid(),
    name: chance.company(),
    logo: 'https://avatar-url.none/get',
    speciality: speciality,
    city: chance.city(),
  },
]);

const server = setupServer(
  rest.get('http://localhost:5050/specialities', (_, res, ctx) =>
    res(ctx.json(specialities))
  ),
  rest.post('http://localhost:5050/company-search', (_, res, ctx) =>
    res(ctx.json(companies))
  ),
  rest.get('https://avatar-url.none/get', (_, res, ctx) => {
    const imageBuffer = fs.readFileSync(
      path.resolve(__dirname, '../test/fixtures/avatar.jpg')
    );

    return res(
      ctx.set('Content-Length', imageBuffer.byteLength.toString()),
      ctx.set('Content-Type', 'image/jpeg'),
      ctx.body(imageBuffer)
    );
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('App flows', () => {
  test('basic workflow', async () => {
    render(<App />);

    await waitFor(() => screen.getByText(companies[0].name));

    const companiesListSection = screen.getByTestId('companies-list');
    const companiesList = within(companiesListSection).getAllByTestId(
      'company-item'
    );
    expect(companiesList.length).toEqual(companies.length);
  });
});
