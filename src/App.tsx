import React, { useEffect, useCallback, useRef, useReducer } from 'react';
import debounce from 'lodash/debounce';

import { CompaniesList } from './Components/CompaniesList';

enum ACTION_TYPES {
  COMPANIES_FETCH = 'fetchCompanies',
  COMPANIES_SUCCESS = 'fetchCompaniesSuccess',
  COMPANIES_FAILURE = 'fetchCompaniesFailure',
  SPECIALITIES_SUCCESS = 'fetchSpecialitiesSuccess',
  SPECIALITIES_FAILURE = 'fetchSpecialitiesFailure',
  SPECIALITIES_FILTER = 'filterSPecialities',
}

export type Company = {
  guid: string;
  name: string;
  logo: string;
  speciality: string;
  city: string;
};

type Action =
  | { type: ACTION_TYPES.COMPANIES_FETCH; payload: { searchQuery: string } }
  | { type: ACTION_TYPES.COMPANIES_SUCCESS; payload: { companies: Company[] } }
  | { type: ACTION_TYPES.COMPANIES_FAILURE; payload: { error: string } }
  | {
      type: ACTION_TYPES.SPECIALITIES_SUCCESS;
      payload: { specialities: string[] };
    }
  | { type: ACTION_TYPES.SPECIALITIES_FAILURE }
  | { type: ACTION_TYPES.SPECIALITIES_FILTER; payload: { speciality: string } };

type State = {
  companies: Company[];
  searchQuery: string;
  error: null | string;
  isLoading: boolean;
  specialitiesFilter: {
    [key: string]: boolean;
  };
};

const initialState: State = {
  companies: [],
  searchQuery: '',
  error: null,
  isLoading: true,
  specialitiesFilter: {},
};

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case ACTION_TYPES.COMPANIES_FETCH:
      return {
        ...state,
        isLoading: true,
        searchQuery: action.payload.searchQuery,
      };
    case ACTION_TYPES.COMPANIES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        companies: action.payload.companies,
        error: null,
      };
    case ACTION_TYPES.COMPANIES_FAILURE:
      return {
        ...state,
        isLoading: false,
        companies: [],
        error: action.payload.error,
      };
    case ACTION_TYPES.SPECIALITIES_SUCCESS:
      return {
        ...state,
        specialitiesFilter: action.payload.specialities.reduce(
          (r, s) => ({ ...r, [s]: true }),
          {}
        ),
      };
    case ACTION_TYPES.SPECIALITIES_FAILURE:
      return {
        ...state,
        specialitiesFilter: {},
      };
    case ACTION_TYPES.SPECIALITIES_FILTER:
      return {
        ...state,
        specialitiesFilter: {
          ...state.specialitiesFilter,
          [action.payload.speciality]: !state.specialitiesFilter[
            action.payload.speciality
          ],
        },
      };
  }
};

function App() {
  const abortController = useRef<AbortController>();
  // TODO: add err and loading handling
  const [
    { companies, searchQuery, specialitiesFilter, error, isLoading },
    dispatch,
  ] = useReducer(reducer, initialState);

  const getCompanies = useCallback(
    debounce((query: string) => {
      abortController.current?.abort();

      const controller = new AbortController();
      abortController.current = controller;

      fetch('http://localhost:5050/company-search', {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        method: 'post',
        body: JSON.stringify({ name: query }),
        signal: controller.signal,
      })
        .then((r) => r.json())
        .then((r) =>
          dispatch({
            type: ACTION_TYPES.COMPANIES_SUCCESS,
            payload: { companies: r },
          })
        )
        .catch((e) => {
          if (e.name === 'AbortError') return;

          dispatch({
            type: ACTION_TYPES.COMPANIES_FAILURE,
            payload: { error: e.message },
          });
        });
    }, 500),
    []
  );

  useEffect(() => {
    getCompanies(searchQuery);
  }, [searchQuery]);

  useEffect(() => {
    fetch('http://localhost:5050/specialities')
      .then((r) => r.json())
      .then((r) =>
        dispatch({
          type: ACTION_TYPES.SPECIALITIES_SUCCESS,
          payload: { specialities: r },
        })
      );
  }, []);

  return (
    <div className="container mx-auto p-4">
      <header>
        <label htmlFor="search">Search: </label>
        <input
          className="border border-gray border-bottom rounded"
          type="text"
          id="search"
          name="search"
          onChange={({ target: { value } }) =>
            dispatch({
              type: ACTION_TYPES.COMPANIES_FETCH,
              payload: { searchQuery: value },
            })
          }
        />
        {Object.keys(specialitiesFilter).length && (
          <>
            {Object.entries(specialitiesFilter).map(([speciality, checked]) => (
              <span className="px-4" key={speciality}>
                <input
                  type="checkbox"
                  id={speciality}
                  name={speciality}
                  checked={checked}
                  onChange={() =>
                    dispatch({
                      type: ACTION_TYPES.SPECIALITIES_FILTER,
                      payload: { speciality },
                    })
                  }
                />
                <label className="pl-1" htmlFor={speciality}>
                  {speciality}
                </label>
              </span>
            ))}
          </>
        )}
      </header>
      <CompaniesList
        companies={companies}
        specialities={Object.entries(specialitiesFilter)
          .filter(([_, selected]) => selected)
          .map(([speciality]) => speciality)}
      />
    </div>
  );
}

export default App;
