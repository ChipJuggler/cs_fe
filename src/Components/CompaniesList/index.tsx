import React from 'react';

import { Company } from '../../App';

export const CompaniesList = ({
  companies,
  specialities,
}: {
  companies: Company[];
  specialities: string[];
}) => (
  <section data-testid="companies-list" className="relative">
    {companies
      .filter((c) => specialities.includes(c.speciality))
      .map((company) => (
        <div
          data-testid="company-item"
          key={company.guid}
          className="flex flex-row border-b border-gray-200 border-solid hower:shadow-md rounded my-2 p-4 items-center"
        >
          <div>
            <img
              className="w-16 rounded-full"
              alt={company.name}
              // FIXME:
              // src={`${company.logo}?random=${company.guid}`}
              src={company.logo}
            />
          </div>
          <div className="font-semibold text-lg px-4 w-5/12">
            {company.name}
          </div>
          <div className="px-4 w-3/12">{company.city}</div>
          <div className="px-4">{company.speciality}</div>
        </div>
      ))}
  </section>
);
